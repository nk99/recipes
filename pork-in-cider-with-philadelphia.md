# Sidre troškinta kiauliena su "Philadelphia" sūriu

4-iems asmenims

## Reikės

- 2 valgomieji šaukštai saulėgrąžų aliejaus
- 8 gabalai (po 100 g) kiaulienos nugarinės
- 4 nekieti obuoliai
- 4 šalotiniai svogūnėliai
- 500 ml obuolių sidro
- 2 valgomieji šaukštai džiovintų šalavijų
- 200 g "Philadelphia Light" sūrio

## Gaminimas

Obuolius nulupkite, išimkite sėklalizdžius ir supjaustykite skiltelėmis. Šalotinius svogūnėlius supjaustykite plonais griežinėliais.

Keptuvėje įkaitinkite aliejų, sudėkite kiaulienos gabalėlius ir iš abiejų pusių po kelias minutes apkepinkite, kol švelniai paruduos. Išimkite iš keptuvės ir padėkite šiltai, kad neatvėstų.

Į keptuvę sudėkite obuolių skilteles ir kelias minutes pakepinkite, kol paruduos. Išimkite iš kepuvės. Tuomet į keptuvę sudėkite šalotinius svogūnėlius ir šalaviją. Pakepinkite kol suminkštės. Supilkite sidrą, sudėkite kiaulieną ir obuolius. Ant lengvos ugnies patroškinkite maždaug 5 minutes, arba tol, kol kiauliena iškeps. Kiaurasamčiu išimkite iš keptuvės kiaulieną bei obuolius ir išdėliokite lėkštėse, kuriose patieksite. Į keptuvėje likusį mišinį įmaišykite "Philadelphia Light" sūrį ir pakaitinkite, kol sūris ištirps. Pagardinkite druska bei pipirais. Užpilkite padažą ant kiaulienos su obuoliais ir patiekite su mėgstamomis šviežiomis daržovėmis.
